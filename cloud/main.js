var BookModel = require('./Book.js');
var PublisherModel = require('./Publisher.js');
var AuthorModel = require('./Author.js');
var util = require('./utils.js');

Parse.Cloud.beforeSave('Book', function beforeHandler(request, response) {
  var object = request.object;
  var runCloudCode = object.get('runCloudCode');

  if (runCloudCode) {
    var data = util.getRecordData(object.attributes);
    var result = util.validate.call(BookModel, data);

    if (result.isValid) {
      response.success();
    } else {
      response.error(result.errors);
    }
  } else {
    response.success();
  }
});

Parse.Cloud.beforeSave('Publisher', function beforeHandler(request, response) {
  var object = request.object;
  var runCloudCode = object.get('runCloudCode');

  if (runCloudCode) {
    var data = util.getRecordData(object.attributes);
    var result = util.validate.call(PublisherModel, data);

    if (result.isValid) {
      response.success();
    } else {
      response.error(result.errors);
    }
  } else {
    response.success();
  }
});

Parse.Cloud.beforeSave('Author', function beforeHandler(request, response) {
  var object = request.object;
  var runCloudCode = object.get('runCloudCode');

  if (runCloudCode) {
    var data = util.getRecordData(object.attributes);
    var result = util.validate.call(AuthorModel, data);

    if (result.isValid) {
      response.success();
    } else {
      response.error(result.errors);
    }
  } else {
    response.success();
  }
});